package dev.smith;

import java.util.Scanner;

import org.apache.log4j.Logger;

import dev.smith.models.*;

public class Project0Driver {
	
	static Logger log = Logger.getRootLogger();
	
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		
		log.info("Start of new session");
		
		
		
		VerifyService ver = new VerifyService();
		//ver.testConnect();
		
		System.out.println("Are you a new user? [y/n]");
		String response = scan.nextLine();
		if(response.equals("y") || response.equals("Y"))
		{
			response = ""; //May be redundant
			System.out.println("Do you wish to create an account? [y/n]");
			response = scan.nextLine();
			if(response.equals("y") || response.equals("Y"))
			{
	//By keeping these within scope, they cannot be used again
				System.out.println("Please enter username");
				String tempUsername = scan.nextLine();
				System.out.println("Please enter password");
				String tempPassword = scan.nextLine();
				
				User tempUser = new User(tempUsername, tempPassword);
				
				log.info("New user created with username " + tempUsername);
				
				//Menu system here
				Menu m = new Menu(tempUser);
				if(tempUser.getType().compareTo("Customer") == 0)
		    		m.menuSystem();
		    	else if(tempUser.getType().compareTo("Employee") == 0)
		    		m.employeeMenu();
			}
		}
		else
		{
			System.out.println("Please enter username");
			String tempUsername = scan.nextLine();      
			System.out.println("Please enter password");
		    String tempPassword = scan.nextLine();
		    
		    //User default constructor to avoid creating a new user.
		    User tempUser = new User();
		    tempUser.setUsername(tempUsername);
		    tempUser.setPassword(tempPassword);
		    
		    if(ver.verify(tempUser)) {
		    	log.info("User " + tempUsername + " successfully logged in.");;
		    	
		    	System.out.println("Welcome back " + tempUser.getUsername());
		    	Menu m = new Menu(tempUser);
		    	if(tempUser.getType().compareTo("Customer") == 0)
		    		m.menuSystem();
		    	else if(tempUser.getType().compareTo("Employee") == 0)
		    		m.employeeMenu();
		    }
		    else {
		    	log.info("User failed to login. Failed username: " + tempUsername);
		    	System.out.println("Sorry, that username or password is not in our system.please try again.");
		    }
		}
		scan.close();
		//ver.stopConnect();
	}
}
