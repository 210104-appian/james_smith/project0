package dev.smith.models;

public enum UserType {
	CUSTOMER, EMPLOYEE, MANAGER
}
