package dev.smith.models;

import java.util.*;

import org.apache.log4j.Logger;

import jdk.internal.org.jline.utils.Log;

import java.io.*;

public class Menu {
	
	private User userValues;
	
	static private Logger log = Logger.getRootLogger();
	
	public Menu(User u)
	{
		userValues = u;
	}
	
	
	//TODO: ADD TRANSFER OPTION
	public void menuSystem()
	{
		Scanner scan = new Scanner(System.in);
		boolean loopControl = true;
		userValues.addAccounts();
		while(loopControl) {
			System.out.println("What would you like to do next?\n"
					+ "[d] deposit,\t[w] withdraw,\t[a] look at accounts"
					+ "\t[c] create new account\t[l] log out");
			String tempCase = scan.nextLine();
			switch (tempCase){
			case "d":
				System.out.println("How much are you depositing?");
				int dep = Integer.parseInt(scan.nextLine());
				userValues.printAccounts();
				System.out.println("Into what account?");
				userValues.deposit(dep, Integer.parseInt(scan.nextLine()));
				break;
			case "w":
				System.out.println("How much are you withdrawing?");
				int wit = Integer.parseInt(scan.nextLine());
				userValues.printAccounts();
				System.out.println("From what account?");
				userValues.withdraw(wit, Integer.parseInt(scan.nextLine()));
				break;
			case "a":
				System.out.println("What account balance do you want to view?");
				userValues.printAccounts();
				int acct = Integer.parseInt(scan.nextLine());
				userValues.getAccountBalance(acct);
				break;
			case "c":
				System.out.println("How much do you want to deposit into"
						+ "the account to start with?");
				double initDeposit = Double.parseDouble(scan.nextLine());
				userValues.addAccount(initDeposit);
				System.out.println("Account is created and awaiting approval.");
				break;
			case "l":
				System.out.println("Thank you, come again!");
				log.info("User " + userValues.getUsername() + " logged out");
				loopControl = false;
				break;
			default:
				System.out.println("Sorry, that's not a valid command. Please try again.");
				break;
			}
		}
	}
	
	public void employeeMenu() {
		Scanner scan = new Scanner(System.in);
		boolean loopControl = true;
		while(loopControl) {
			System.out.println("What would you like to do next?\n"
					+ "[a] approve an account,\t[r] reject an account,\t[c] look at accounts for a customer"
					+ "\t[t] view all transactions,\t[l] log out");
			String tempCase = scan.nextLine();
			switch (tempCase){
			case "a":
				System.out.println("What is the username for the account?");
				String tempUsername = scan.nextLine();
				System.out.println("What is the account number?");
				userValues.printUsersAccounts(tempUsername);
				userValues.approveDeny(Integer.parseInt(scan.nextLine()), true);
				break;
			case "r":
				System.out.println("What is the username for the account?");
				String tempUser = scan.nextLine();
				System.out.println("What is the account number?");
				userValues.printUsersAccounts(tempUser);
				userValues.approveDeny(Integer.parseInt(scan.nextLine()), false);
				break;
			case "c":
				System.out.println("What is the username for the account?");
				String tempName = scan.nextLine();
				System.out.println("What is the account number?");
				userValues.printUsersAccounts(tempName);
				userValues.getUserAccounts(Integer.parseInt(scan.nextLine()));;
				break;
			case "t":
				FileReader fr = null;
				String fromFile;
				try {
					fr = new FileReader("src/main/java/log.txt");
					BufferedReader br = new BufferedReader(fr);
					while((fromFile = br.readLine()) != null) {
						System.out.println(fromFile);
					}
				} catch (FileNotFoundException e) {
					System.out.println("File destination is invalid");
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
				break;
			case "l":
				log.info("User " + userValues.getUsername() + " logged out");
				loopControl = false;
				break;
			default:
				System.out.println("Sorry, that's not a valid command. Please try again.");
				break;
			}
		}
	}
}
