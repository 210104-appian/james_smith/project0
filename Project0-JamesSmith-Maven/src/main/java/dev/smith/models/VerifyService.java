package dev.smith.models;

import java.sql.*;

public class VerifyService {
	//1. Connect to database
	//2. Verify ID
	//3. Verify password
	//4. Get User type
	//5. Get User bank balance/permissions
	//6. Receive user input for what else the user wants to do
	//7. Verify input with permissions
	//8. Close the connection
	
	public String url;
	public String username;
	public String password;
	
	public VerifyService() {
		url = System.getenv("DB_URL");
		username = System.getenv("DB_USERNAME");
		password = System.getenv("DB_PASSWORD");
	}
	
	//Didn't realize at first that this has to be called every time, should work for a junit test though.
	public void testConnect()
	{
		//connect;
		try {
			Connection connection = DriverManager.getConnection(url, username, password);
			System.out.println(connection.getMetaData().getDriverName());
		} catch (SQLException e) {
			System.out.println("Could not connect.");
			e.printStackTrace();
		}
	}
	
	public boolean verify(User u)
	{
		/*for(element : database)
		 * {if(element.username == u.getUsername && 
		 * element.password == u.GetPassword)
		 * for(accounts : element.useraccounts)
		 * u.addaccount(accounts.accountbalance)
		 * u.type = element.type
		 * return true;
		 * }
		 * return false;
		 */
		try {
			Connection connection = DriverManager.getConnection(url, username, password);
			//System.out.println(connection.getMetaData().getDriverName());
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery("SELECT * FROM USERS");
			while(resultSet.next())
			{
				String username = resultSet.getString("USERNAME");
				//System.out.println(username.compareTo(u.getUsername()) == 0);
				String password = resultSet.getString("PASSWORD");
				//System.out.println(password.compareTo(u.getPassword()) == 0);
				if(password.compareTo(u.getPassword()) == 0 && 
						username.compareTo(u.getUsername()) == 0) {
					System.out.println("Login successful!");
					switch(resultSet.getString("USERTYPE")) {
					case "EMPLOYEE":
						u.type = UserType.EMPLOYEE;
						break;
					case "CUSTOMER":
						u.type = UserType.CUSTOMER;
						break;
					case "MANAGER":
						u.type = UserType.MANAGER;
						break;
					default:
						u.type = UserType.CUSTOMER;
						break;
					}
					return true;
				}
			}
			
		} catch (SQLException e) {
			System.out.println("Could not connect.");
			e.printStackTrace();
		}
		
		return false;
	}
	
	public int getBankBalance(String uName, String pWord) {
		return 0;
	}
	
	public void stopConnect()
	{
		
	}
}
