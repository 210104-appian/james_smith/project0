package dev.smith.models;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import java.sql.*;

public class User {
	
	static private Logger log = Logger.getRootLogger();
	
	public UserType type;	
	private int uID;
	private String username, password, transactionHistory = "";
	public ArrayList<Integer> accts; //List of all accounts the user has
	
	VerifyService ver = new VerifyService();
	String url = ver.url;
	String dPassword = ver.password;
	String dUsername = ver.username;
	
	//This is how we initialize an empty user to test with the database
	public User(){
		type = UserType.CUSTOMER;
		uID = 0;
		accts = new ArrayList<Integer>();
	}
	
	//This will be the way we create a new user for the database
	public User(String username, String password)
	{
		type = UserType.CUSTOMER;
		//uID = User.nextID++;
		this.username = username;
		this.password = password;
		accts = new ArrayList<Integer>();
		//Add in connection stuff here		
		
		try {
			Connection connection = DriverManager.getConnection(url, dUsername, dPassword);
			//System.out.println(connection.getMetaData().getDriverName());
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery("SELECT * FROM USERS");
			while(resultSet.next()){
				if(resultSet.getInt("USER_ID")+1 > uID)
					uID = resultSet.getInt("USER_ID")+1;				
			}
			resultSet.close();
			
			//UserID, Username, password, Usertype
			String sqlStatement = "INSERT INTO USERS VALUES (?, ?, ?, ?)";
			PreparedStatement pStatement = connection.prepareStatement(sqlStatement);
			pStatement.setInt(1, uID);
			pStatement.setString(2, username);
			pStatement.setString(3,  password);
			pStatement.setString(4, type.name());
			pStatement.execute();
			
			
			
		} catch (SQLException e) {
			System.out.println("Could not connect.");
			e.printStackTrace();
		}
	}
	
	public double deposit(double d, int acct){
		
		log.info("User " + username + " attempted to deposit $" + d + " into user's account " + acct);
		
		if(acct-1 < 0 || acct-1 >= accts.size()){
			System.out.println("Sorry, that is not a valid account number.");
			return 0.0;
		}
		try {
			//System.out.println("url: " + url + "\n username: " + dUsername + "\n password: " + dPassword);
			Connection connection = DriverManager.getConnection(url, dUsername, dPassword);
			//Need to add all account ID's to this User
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery("SELECT * FROM ACCOUNTS");
			while(resultSet.next()){
				int id = resultSet.getInt("ACCOUNT_ID");
				if(id == accts.get(acct-1)){
					transactionHistory += "User " + username + " deposited $" + d + "from account #" + id + "\n";
					
					//First, we grab the balance
					double balance = resultSet.getDouble("BALANCE");
					
					//Then, add the amount that needs to get withdrawn from the balance
					if(d > 0) {
						balance += d;
						
						//Then, replace the current balance in the db with the new one
						String sqlStatement = "UPDATE ACCOUNTS SET BALANCE = ? WHERE ACCOUNT_ID = ?";
						PreparedStatement pStatement = connection.prepareStatement(sqlStatement);
						pStatement.setDouble(1, balance);
						pStatement.setInt(2, id);
						pStatement.execute();
						return balance;
					}
					else{
						System.out.println("Sorry, that's not a valid amount, please try again.");
						return 0.0;
					}
				}
			}
			resultSet.close();
		}	catch (SQLException e) {
			System.out.println("Could not connect.");
			e.printStackTrace();
		}
		return 0.0;
	}
	
	public double withdraw(double d, int acct){
		log.info("User " + username + " attempted to withdraw $" + d + " into user's account " + acct);
		if(acct-1 <0 || acct-1 >= accts.size()){
			System.out.println("Sorry, that is not a valid account number.");
			return 0.0;
		}
		try {
			//System.out.println("url: " + url + "\n username: " + dUsername + "\n password: " + dPassword);
			Connection connection = DriverManager.getConnection(url, dUsername, dPassword);
			//Need to add all account ID's to this User
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery("SELECT * FROM ACCOUNTS");
			while(resultSet.next()){
				int id = resultSet.getInt("ACCOUNT_ID");
				if(id == accts.get(acct-1)){
					transactionHistory += "User " + username + " withdrew $" + d + "from account #" + id + "\n";
					
					//First, we grab the balance
					double balance = resultSet.getDouble("BALANCE");
					
					//Then, subtract the amount that needs to get withdrawn from the balance
					if(balance >= d) {
						balance -= d;
						
						//Then, replace the current balance in the db with the new one
						String sqlStatement = "UPDATE ACCOUNTS SET BALANCE = ? WHERE ACCOUNT_ID = ?";
						PreparedStatement pStatement = connection.prepareStatement(sqlStatement);
						pStatement.setDouble(1, balance);
						pStatement.setInt(2, id);
						pStatement.execute();
						return balance;
					}
					else{
						log.info("User " + username + " did not have enough funds to withdraw from their account.");
						System.out.println("Sorry, that's not a valid amount, please try again.");
						return 0.0;
					}
				}
			}
			resultSet.close();
		}	catch (SQLException e) {
			System.out.println("Could not connect.");
			e.printStackTrace();
		}
		//return accts.get(acct).withdraw(d);
		return 0.0;
	}
	
	public void addAccount(double bBalance)
	{
		//accts.add(new BankAccount(this, bBalance));
		try {
			//System.out.println("url: " + url + "\n username: " + dUsername + "\n password: " + dPassword);
			Connection connection = DriverManager.getConnection(url, dUsername, dPassword);
			
			//First, create the new account
			//Account Id, Approval, Balance
			String sqlStatement = "INSERT INTO ACCOUNTS(ACCOUNT_ID, APPROVAL, BALANCE) VALUES (?, ?, ?)";
			PreparedStatement pStatement = connection.prepareStatement(sqlStatement);
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery("SELECT * FROM ACCOUNTS");
			int aID = 0;
			if(!resultSet.isBeforeFirst())
			{
				aID = 1;
			}
			else
			{	
				
				while(resultSet.next()){
					if(resultSet.getInt("ACCOUNT_ID")+1 > aID)
							aID = resultSet.getInt("ACCOUNT_ID")+1;
					//System.out.println(aID);
				}
				//System.out.println(aID);
			}
			
			log.info("New account " + aID + " under user " + username + " was created with an initial deposit of " + bBalance);
			
			resultSet.close();
			pStatement.setInt(1, aID);
			pStatement.setString(2, "FALSE"); //Hard-code approval to be false, we always want an employee to verify
			pStatement.setDouble(3, bBalance);
			pStatement.execute();

			sqlStatement = "INSERT INTO USER_ACCOUNTS VALUES (?, ?)";
			pStatement = connection.prepareStatement(sqlStatement);
			pStatement.setInt(1, uID);
			pStatement.setInt(2, aID);
			pStatement.execute();
		} catch (SQLException e) {
			System.out.println("Could not connect.");
			e.printStackTrace();
		}
		
		addAccounts();
	}
	
	public void addAccounts()
	{
		try {
			//System.out.println("url: " + url + "\n username: " + dUsername + "\n password: " + dPassword);
			Connection connection = DriverManager.getConnection(url, dUsername, dPassword);
			//Need to add all account ID's to this User
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery("SELECT * FROM USER_ACCOUNTS");
			while(resultSet.next()){
				int id = resultSet.getInt("USER_ID");
				if(id == uID){
					if(!accts.contains(resultSet.getInt("ACCOUNT_ID"))){
						accts.add(resultSet.getInt("ACCOUNT_ID"));
					}
				}
			}
			resultSet.close();
		}	catch (SQLException e) {
			System.out.println("Could not connect.");
			e.printStackTrace();
		}
	}
	
	//This should be called every time the user is prompted which account they are requesting info about
	public void printAccounts(){
		for(int i = 0; i < accts.size(); i++) {
			System.out.println("Account #" + (i+1));
		}
	}
	
	public void printUsersAccounts(String username) {
		try {
			//System.out.println("url: " + url + "\n username: " + dUsername + "\n password: " + dPassword);
			Connection connection = DriverManager.getConnection(url, dUsername, dPassword);
			//Need to add all account ID's to this User
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery("SELECT * FROM USERS");
			while(resultSet.next()){
				String uName = resultSet.getString("USERNAME");
				if(uName.compareTo(username) == 0){
					int tempID = resultSet.getInt("USER_ID");
					resultSet.close();
					resultSet = statement.executeQuery("SELECT * FROM USER_ACCOUNTS");
					String accountIDs = "";
					while(resultSet.next()) {
						if(resultSet.getInt("USER_ID") == tempID)
							accountIDs += "\t" + resultSet.getInt("ACCOUNT_ID");
					}
					System.out.println(accountIDs);
				}
			}
			resultSet.close();
		}	catch (SQLException e) {
			System.out.println("Could not connect.");
			e.printStackTrace();
		}
	}
	
	//Get account balance for customers
	public void getAccountBalance(int acctID)
	{
		//return accts.get(acctID).getBalance(); Deprecated, trying to remove userdata from Java completely
		if(acctID-1 <0 || acctID-1 >= accts.size()){
			System.out.println("Sorry, that is not a valid account number.");
		}
		else {
			try {
				//System.out.println("url: " + url + "\n username: " + dUsername + "\n password: " + dPassword);
				Connection connection = DriverManager.getConnection(url, dUsername, dPassword);
				//Need to add all account ID's to this User
				Statement statement = connection.createStatement();
				ResultSet resultSet = statement.executeQuery("SELECT * FROM ACCOUNTS");
				while(resultSet.next()){
					int id = resultSet.getInt("ACCOUNT_ID");
					if(id == accts.get(acctID-1)){
						System.out.println("Account balance for account " 
						+ acctID + " is $" + resultSet.getDouble("BALANCE"));
					}
				}
				resultSet.close();
			}	catch (SQLException e) {
				System.out.println("Could not connect.");
				e.printStackTrace();
			}
		}
		//return 0.0;
	}
	
	public void approveDeny(int accountID, boolean approval){
		try {
			//System.out.println("url: " + url + "\n username: " + dUsername + "\n password: " + dPassword);
			Connection connection = DriverManager.getConnection(url, dUsername, dPassword);
			//Need to add all account ID's to this User
			
					String sqlStatement = "UPDATE ACCOUNTS SET APPROVAL = ? WHERE ACCOUNT_ID = ?";
					PreparedStatement pStatement = connection.prepareStatement(sqlStatement);
					if(approval) {
						pStatement.setString(1, "TRUE");
						log.info("Account " + accountID + " was approved by employee " + username);
					}
					else {
						pStatement.setString(1, "FALSE");
						log.info("Account " + accountID + " was approved by employee " + username);
					}
					pStatement.setInt(2, accountID);
					pStatement.execute();
		
		}	catch (SQLException e) {
			System.out.println("Could not connect.");
			e.printStackTrace();
		}
	}
	
	//Get the account balance for employees
	public void getUserAccounts(int actID) {
		try {
			//System.out.println("url: " + url + "\n username: " + dUsername + "\n password: " + dPassword);
			Connection connection = DriverManager.getConnection(url, dUsername, dPassword);
			//Need to add all account ID's to this User
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery("SELECT * FROM ACCOUNTS");
			while(resultSet.next()){
				int uAct = resultSet.getInt("ACCOUNT_ID");
				if(uAct == actID){
					System.out.println("Balance for account " + uAct + " is $" + resultSet.getDouble("BALANCE"));
				}
			}
			resultSet.close();
		}	catch (SQLException e) {
			System.out.println("Could not connect.");
			e.printStackTrace();
		}
	}
	
	public void viewAllTransactions() {
		
	}
	
	public long getuID() {
		return uID;
	}

	public void setuID(int uID) {
		this.uID = uID;
	}

	public String getUsername() {
		return username;
	}
	
	public String getPassword() { //Not sure about this one, but will keep it this way for now
		return password;
	}
	
	public void setUsername(String username) {
		this.username = username;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
	
	public boolean checkPassword(String password) {
		return this.password.equals(password);
	}
	
	void printTransactionHistory(){
		System.out.println(transactionHistory);
	}
	
	public String getType() {
		switch(type) {
		case EMPLOYEE:
			return "Employee";
		case CUSTOMER:
			return "Customer";
		case MANAGER:
			return "Manager";
		default:
			return "";
		}
	}

	@Override
	public String toString() {
		return "User [type=" + type + ", uID=" + uID + ", username=" + username + ", password=" + password
				+ ", transactionHistory=" + transactionHistory + ", accts=" + accts + "]";
	}


	
}
