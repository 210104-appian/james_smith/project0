Project 0 Specs

-------------------------

Customers can:
	-Create any number of new accounts
	-See the balance of any accounts belonging to them
	-Deposit/withdraw to any of their accounts

-------------------------

Employees can:
	-Approve/deny any account
	-Look at the balance of any account
	-View all transactions